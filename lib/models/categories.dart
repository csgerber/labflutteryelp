
class Categories {

  final String alias;
  final String title;

	Categories.fromJsonMap(Map<String, dynamic> map): 
		alias = map["alias"],
		title = map["title"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['alias'] = alias;
		data['title'] = title;
		return data;
	}
}
