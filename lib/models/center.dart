
class Center {

  final double longitude;
  final double latitude;

	Center.fromJsonMap(Map<String, dynamic> map): 
		longitude = map["longitude"],
		latitude = map["latitude"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['longitude'] = longitude;
		data['latitude'] = latitude;
		return data;
	}
}
