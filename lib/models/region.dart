import 'package:yelper3/models/center.dart';

class Region {

  final Center center;

	Region.fromJsonMap(Map<String, dynamic> map): 
		center = Center.fromJsonMap(map["center"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['center'] = center == null ? null : center.toJson();
		return data;
	}
}
