import 'package:yelper3/models/businesses.dart';
import 'package:yelper3/models/region.dart';

class YelpResponse {

  final List<Businesses> businesses;
  final int total;
  final Region region;

	YelpResponse.fromJsonMap(Map<String, dynamic> map): 
		businesses = List<Businesses>.from(map["businesses"].map((it) => Businesses.fromJsonMap(it))),
		total = map["total"],
		region = Region.fromJsonMap(map["region"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['businesses'] = businesses != null ? 
			this.businesses.map((v) => v.toJson()).toList()
			: null;
		data['total'] = total;
		data['region'] = region == null ? null : region.toJson();
		return data;
	}
}
