import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:yelper3/models/businesses.dart';
import 'package:yelper3/models/dummy.dart';
import 'package:flutter/cupertino.dart';
import 'package:yelper3/screens/search_screen.dart';
import 'package:yelper3/widgets/business_widget.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}


class _HomeState extends State<Home> {

  //our members that will change
  List<Businesses> list_businesses = [];


  //render the businesses
  Widget renderBusinesses(BuildContext context, int index) {
    Businesses business = list_businesses[index];
    return BusinessWidget(business, (Businesses business) => alertDialog(context, business));
  }

  searchBusinesses() async {
    Businesses business = await Navigator.push(context,
        CupertinoPageRoute(builder: (BuildContext context) {
          return SearchScreen();
        }));
    // in case user go back before searching and tapping the result,
    // it shouldn't add the null value to the businesses property
    if (business != null) {
      setState(() {
        list_businesses.add(business);
      });
    }
  }


  //http://androidkt.com/flutter-alertdialog-example/
  alertDialog(BuildContext context,  Businesses biz) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(biz.name),
          content: Text(biz.toString()),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }




  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Title(
          child: Text("Favorite Places"),
          color: Colors.white,
        ),
      ),
      body: ListView.builder(
        itemBuilder: this.renderBusinesses,
        itemCount: list_businesses.length,
      ),
      floatingActionButton: Align(
        alignment: Alignment.bottomRight,
        child: Padding(
          padding: const EdgeInsets.all(30),
          child: FloatingActionButton(

            onPressed: () => this.searchBusinesses(),
            child: Icon(Icons.add),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
