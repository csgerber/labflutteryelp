import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:yelper3/models/businesses.dart';
import 'package:yelper3/models/yelp_response.dart';
import 'package:yelper3/services/yelp3.dart';
import 'package:yelper3/models/dummy.dart';
import 'package:yelper3/widgets/business_widget.dart';

class SearchResultScreen extends StatelessWidget {
  final String location;
  final String term;
  SearchResultScreen({this.location, this.term});



  Widget renderResult(YelpResponse response) {
    return ListView.builder(
        itemBuilder: (BuildContext context, index) {
          //set a breakpoint here - there is an exception
          Businesses businesses = response.businesses[index];
          return BusinessWidget(businesses, (Businesses businesses) {
           // Dummy dummy = new Dummy(business.name, business.phone);
            Navigator.pop(context, businesses);
          });
        },
        itemCount: response.businesses.length);
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Title(
          child: Text("Search Result"),
          color: Colors.white,
        ),
      ),
      body: FutureBuilder(
        future: YelpService().searchBusiness(term: term, location: location),
        builder: (BuildContext context, AsyncSnapshot<YelpResponse> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.active:
            case ConnectionState.waiting:
            case ConnectionState.none:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              return this.renderResult(snapshot.data);
          }
        },
      ),
    );
  }
}