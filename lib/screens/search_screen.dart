import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:yelper3/models/businesses.dart';
import 'package:yelper3/models/dummy.dart';
import 'package:yelper3/screens/search_result_screen.dart';

class SearchScreen extends StatefulWidget {
  SearchScreenState createState() => SearchScreenState();
}

class SearchScreenState extends State<SearchScreen> {
  //here are our members, or model
  //set them with some initial state
  String location = "Chicago";
  String term = "House Pub";

  //these are the entities do the changing, or controllers
  TextEditingController termController;
  TextEditingController locationController;

  //this uses M-V-C
  @override
  initState() {
    super.initState();

    termController = TextEditingController(
      text: term,
    );
    termController.addListener(() {
      setState(() {
        term = termController.text;
      });
    });
    locationController = TextEditingController(
      text: location,
    );
    locationController.addListener(() {
      setState(() {
        location = locationController.text;
      });
    });
  }

  search() async {
    //this will push a new route onto the stack
    Businesses businesses = await Navigator.push(context,
        CupertinoPageRoute(builder: (BuildContext context) {
          return SearchResultScreen(
            location: location,
            term: term,
          );
        }));
    //when this returns, it'll simply skip this page and go back to the grandparent
    Navigator.pop(context, businesses);
  }




  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Title(
          child: Text("Search Places"),
          color: Colors.white,
        ),
      ),
      body: Container(
          padding: EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              buildNameColumn(),
              buildLocationColumn(),
              buildSearchButton()
            ],
          )),
    );
  }

  Column buildLocationColumn() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Container(child: Text("Location")),
              TextFormField(
                controller: locationController,
                decoration:
                InputDecoration(hintText: "e.g. Bruxelles or Chicago or 98101"),
              )
            ]);
  }

  Column buildNameColumn() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Container(child: Text("Name")),
              TextFormField(
                controller: termController,
                decoration: InputDecoration(hintText: "e.g. Pleasant Hub"),
              )
            ]);
  }

  FlatButton buildSearchButton() {
    return FlatButton(
              color: Colors.blue,
              onPressed: this.search,
              child: Row(
                children: <Widget>[
                  Icon(Icons.search),
                  Text("Search"),
                ],
              ),
            );
  }
}
