import 'package:yelper3/models/yelp_response.dart';
import 'package:dio/dio.dart';

class YelpService {
  static const String YELP_API_KEY =
      'FxVS9ym_GgG992_zBLzSvjNubU2PEwA8ERyfEGJKYZVdK7A1EWXRDmr2RHQ3jb3XcdOTTVYaPC_ITHITXDjRjF7hYlEdMcT5hXtNji97JQBAncJn4yeloWeTPFuzXHYx';
  Dio get client {
    return Dio(BaseOptions(baseUrl: 'https://api.yelp.com/v3/', headers: {
      'Authorization': 'Bearer $YELP_API_KEY',
    }));
  }

  Future<YelpResponse> searchBusiness(
      {String term, String location, int radius = 1000}) async {
    Response<Map<String, Object>> response = await client
        .get('businesses/search', queryParameters: {
      'term': term,
      'location': location,
      'radius': radius
    });
    return YelpResponse.fromJsonMap(response.data);
  }
}
