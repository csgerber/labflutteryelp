import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:yelper3/models/businesses.dart';

class BusinessWidget extends StatelessWidget {
  final Businesses business;
  final Function onTapResult;
  BusinessWidget(this.business, this.onTapResult);
  Widget reviewStars(Businesses business) {
    List<Widget> children = [];
    int baseStar = business.rating.floor();
    bool hasHalf = business.rating - baseStar > 0;
    for (int i = 1; i <= baseStar; i += 1) {
      children.add(Icon(Icons.star));
    }
    if (hasHalf) {
      children.add(Icon(Icons.star_half));
    }
    int rest = 5 - children.length;
    for (int j = 1; j <= rest; j += 1) {
      children.add(Icon(Icons.star_border));
    }

    return Row(
      children: children,
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
        onTap: () => onTapResult(business),
        child: Container(
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.only(bottom: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(right: 10),
                    child: Image(
                        image: NetworkImage(business.image_url), width: 100)),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(business.name),
                      Text(business.categories
                          .map((c) => c.title)
                          .toList()
                          .join(", ")),
                      Text([business.location.address1, business.location.city]
                          .join(",")),
                      this.reviewStars(business),
                    ],
                  ),
                ),
              ],
            )));
  }
}
