{
    "businesses": [
        {
            "id": "AKOxQTqo_t8V0LwsXXw-xA",
            "alias": "pub-royale-chicago",
            "name": "Pub Royale",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/kIXfU1jkHXADHrneTeZUHw/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/pub-royale-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 383,
            "categories": [
                {
                    "alias": "pubs",
                    "title": "Pubs"
                },
                {
                    "alias": "british",
                    "title": "British"
                },
                {
                    "alias": "indpak",
                    "title": "Indian"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 41.90286,
                "longitude": -87.67916
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "2049 W Division",
                "address2": null,
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "2049 W Division",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17736616874",
            "display_phone": "(773) 661-6874",
            "distance": 278.404435816512
        },
        {
            "id": "L0SpO9Eb6fHIZ5ljKpdAow",
            "alias": "queen-mary-tavern-chicago",
            "name": "Queen Mary Tavern",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/UK8Y1y4yt5po724RszRzUw/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/queen-mary-tavern-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 75,
            "categories": [
                {
                    "alias": "bars",
                    "title": "Bars"
                }
            ],
            "rating": 4.5,
            "coordinates": {
                "latitude": 41.90283,
                "longitude": -87.68073
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "2125 W Division St",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "2125 W Division St",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17736973522",
            "display_phone": "(773) 697-3522",
            "distance": 346.51312491457077
        },
        {
            "id": "iOMvXQYFl6_mtw0wX6ZUXw",
            "alias": "forbidden-root-chicago",
            "name": "Forbidden Root",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/6Mt0NHK1ovmRSMIXefQuwA/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/forbidden-root-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 409,
            "categories": [
                {
                    "alias": "breweries",
                    "title": "Breweries"
                },
                {
                    "alias": "newamerican",
                    "title": "American (New)"
                },
                {
                    "alias": "burgers",
                    "title": "Burgers"
                }
            ],
            "rating": 4.5,
            "coordinates": {
                "latitude": 41.8962684049794,
                "longitude": -87.6713814117853
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "1746 W Chicago Ave",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1746 W Chicago Ave",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+13129292202",
            "display_phone": "(312) 929-2202",
            "distance": 1120.9272713269957
        },
        {
            "id": "bEhKMjnmpZLtFb7oAKjYYA",
            "alias": "fatpour-tap-works-chicago",
            "name": "Fatpour Tap Works",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/CFzniZDYKRDYm3jZCRpKyg/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/fatpour-tap-works-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 558,
            "categories": [
                {
                    "alias": "sportsbars",
                    "title": "Sports Bars"
                },
                {
                    "alias": "newamerican",
                    "title": "American (New)"
                },
                {
                    "alias": "pizza",
                    "title": "Pizza"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 41.90289,
                "longitude": -87.67762
            },
            "transactions": [
                "restaurant_reservation"
            ],
            "price": "$$",
            "location": {
                "address1": "2005 W Division St",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "2005 W Division St",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17736988940",
            "display_phone": "(773) 698-8940",
            "distance": 245.546342119858
        },
        {
            "id": "kNyNZE7-6vgrMtaeS1sOVw",
            "alias": "links-taproom-chicago",
            "name": "Links Taproom",
            "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/7t_ZKtn14l6M74EJaNgCag/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/links-taproom-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 232,
            "categories": [
                {
                    "alias": "beerbar",
                    "title": "Beer Bar"
                }
            ],
            "rating": 4.5,
            "coordinates": {
                "latitude": 41.9098596489117,
                "longitude": -87.6763864489499
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "1559 N Milwaukee Ave",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1559 N Milwaukee Ave",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17733607692",
            "display_phone": "(773) 360-7692",
            "distance": 534.9626108549475
        },
        {
            "id": "CtTgpVKIE7dzdalZyFxgCQ",
            "alias": "innertown-pub-chicago",
            "name": "Innertown Pub",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/NY11uu98HfigOuxA1hjopA/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/innertown-pub-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 211,
            "categories": [
                {
                    "alias": "pubs",
                    "title": "Pubs"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 41.90115,
                "longitude": -87.67613
            },
            "transactions": [],
            "price": "$",
            "location": {
                "address1": "1935 W Thomas St",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1935 W Thomas St",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17732359795",
            "display_phone": "(773) 235-9795",
            "distance": 460.59968390026324
        },
        {
            "id": "fLwNhXatcSoQEI8XeFbrtg",
            "alias": "bangers-and-lace-chicago-2",
            "name": "Bangers & Lace",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/QDj_QEiVSq_BxUAxP8PKEw/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/bangers-and-lace-chicago-2?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 573,
            "categories": [
                {
                    "alias": "pubs",
                    "title": "Pubs"
                },
                {
                    "alias": "cocktailbars",
                    "title": "Cocktail Bars"
                },
                {
                    "alias": "breakfast_brunch",
                    "title": "Breakfast & Brunch"
                }
            ],
            "rating": 3.5,
            "coordinates": {
                "latitude": 41.90357,
                "longitude": -87.67023
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "1670 W Division St",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1670 W Division St",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17732526499",
            "display_phone": "(773) 252-6499",
            "distance": 646.4264914801028
        },
        {
            "id": "niSgEwYfDqy0-DCyCs8pqQ",
            "alias": "pint-pub-kitchen-chicago-4",
            "name": "Pint Pub + Kitchen",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/7kHqudzIv1K6iqp1-_Ot4A/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/pint-pub-kitchen-chicago-4?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 352,
            "categories": [
                {
                    "alias": "irish_pubs",
                    "title": "Irish Pub"
                }
            ],
            "rating": 3,
            "coordinates": {
                "latitude": 41.90974,
                "longitude": -87.676
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "1547 N Milwaukee Ave",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1547 N Milwaukee Ave",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17737720990",
            "display_phone": "(773) 772-0990",
            "distance": 529.8806887002494
        },
        {
            "id": "PodX69Zho5y2Nl1h0SeIlQ",
            "alias": "happy-village-chicago",
            "name": "Happy Village",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/VX32LHhSfhouZtnq-j7H-g/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/happy-village-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 258,
            "categories": [
                {
                    "alias": "divebars",
                    "title": "Dive Bars"
                },
                {
                    "alias": "beergardens",
                    "title": "Beer Gardens"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 41.901296107314,
                "longitude": -87.6745942609487
            },
            "transactions": [],
            "price": "$",
            "location": {
                "address1": "1059 N Wolcott Ave",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1059 N Wolcott Ave",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17734861512",
            "display_phone": "(773) 486-1512",
            "distance": 503.3657735242283
        },
        {
            "id": "KgAf-wDVFEb1tT1HV-EqIA",
            "alias": "whiskey-business-and-fry-bar-chicago",
            "name": "Whiskey Business & Fry Bar",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/V4CantCwZ2UvurnI_Wgn5Q/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/whiskey-business-and-fry-bar-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 212,
            "categories": [
                {
                    "alias": "whiskeybars",
                    "title": "Whiskey Bars"
                },
                {
                    "alias": "beergardens",
                    "title": "Beer Gardens"
                }
            ],
            "rating": 3,
            "coordinates": {
                "latitude": 41.9068374633789,
                "longitude": -87.6714477539062
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "1367 N Milwaukee Ave",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1367 N Milwaukee Ave",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17736987362",
            "display_phone": "(773) 698-7362",
            "distance": 543.6410367819177
        },
        {
            "id": "qSq0DMpWgfVUIyPI8lag_Q",
            "alias": "louies-pub-chicago",
            "name": "Louie's Pub",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/opaDHbQRzvdsxdMoyk4fYw/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/louies-pub-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 182,
            "categories": [
                {
                    "alias": "karaoke",
                    "title": "Karaoke"
                },
                {
                    "alias": "pubs",
                    "title": "Pubs"
                }
            ],
            "rating": 3.5,
            "coordinates": {
                "latitude": 41.910504,
                "longitude": -87.669977
            },
            "transactions": [],
            "price": "$",
            "location": {
                "address1": "1659 W North Ave",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1659 W North Ave",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17732277947",
            "display_phone": "(773) 227-7947",
            "distance": 876.6695111568444
        },
        {
            "id": "jhD3YftmGYeZtI1zbaohIQ",
            "alias": "the-tankard-chicago",
            "name": "The Tankard",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/6YVM9Ak46C_9sWBtS8pemA/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/the-tankard-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 59,
            "categories": [
                {
                    "alias": "tradamerican",
                    "title": "American (Traditional)"
                },
                {
                    "alias": "burgers",
                    "title": "Burgers"
                },
                {
                    "alias": "cocktailbars",
                    "title": "Cocktail Bars"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 41.9112526,
                "longitude": -87.6784449
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "1635 N Milwaukee Ave",
                "address2": "",
                "address3": null,
                "city": "Chicago",
                "zip_code": "60647",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1635 N Milwaukee Ave",
                    "Chicago, IL 60647"
                ]
            },
            "phone": "+17732922765",
            "display_phone": "(773) 292-2765",
            "distance": 679.8809645442034
        },
        {
            "id": "z_jQAXL_MWorV9NBvBNd3Q",
            "alias": "nildas-place-chicago-2",
            "name": "Nilda's Place",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/znxkldEdnZPZYvOwrwTMIg/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/nildas-place-chicago-2?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 26,
            "categories": [
                {
                    "alias": "divebars",
                    "title": "Dive Bars"
                }
            ],
            "rating": 4.5,
            "coordinates": {
                "latitude": 41.89799,
                "longitude": -87.67448
            },
            "transactions": [],
            "price": "$",
            "location": {
                "address1": "1858 W Iowa St",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1858 W Iowa St",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "",
            "display_phone": "",
            "distance": 834.0886075795693
        },
        {
            "id": "GnAltFPAh38FJO7wXpi1Ww",
            "alias": "gold-star-bar-chicago",
            "name": "Gold Star Bar",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/1gMCxuDKNG4CSSw-YjI61Q/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/gold-star-bar-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 207,
            "categories": [
                {
                    "alias": "bars",
                    "title": "Bars"
                },
                {
                    "alias": "poolhalls",
                    "title": "Pool Halls"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 41.902961730957,
                "longitude": -87.6720428466797
            },
            "transactions": [],
            "price": "$",
            "location": {
                "address1": "1755 W Division St",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1755 W Division St",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17732278700",
            "display_phone": "(773) 227-8700",
            "distance": 529.7087959116543
        },
        {
            "id": "zF9o33BVqLUOcCmnuB2eQw",
            "alias": "handlebar-chicago",
            "name": "Handlebar",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/kuxyTzQzy0UMDP-FVvIEHQ/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/handlebar-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 1340,
            "categories": [
                {
                    "alias": "vegetarian",
                    "title": "Vegetarian"
                },
                {
                    "alias": "bars",
                    "title": "Bars"
                },
                {
                    "alias": "vegan",
                    "title": "Vegan"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 41.9100899,
                "longitude": -87.6853
            },
            "transactions": [
                "pickup",
                "delivery"
            ],
            "price": "$$",
            "location": {
                "address1": "2311 W North Ave",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60647",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "2311 W North Ave",
                    "Chicago, IL 60647"
                ]
            },
            "phone": "+17733849546",
            "display_phone": "(773) 384-9546",
            "distance": 830.2762406580971
        },
        {
            "id": "XRS2VK4wBanmYFHeITTc4A",
            "alias": "the-barrel-run-chicago",
            "name": "The Barrel Run",
            "image_url": "https://s3-media3.fl.yelpcdn.com/bphoto/tOWRNSw2Yk04FYkPBsYeQg/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/the-barrel-run-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 38,
            "categories": [
                {
                    "alias": "bustours",
                    "title": "Bus Tours"
                },
                {
                    "alias": "breweries",
                    "title": "Breweries"
                },
                {
                    "alias": "distilleries",
                    "title": "Distilleries"
                }
            ],
            "rating": 5,
            "coordinates": {
                "latitude": 41.8928448806887,
                "longitude": -87.63122072047
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "",
                "address2": null,
                "address3": "",
                "city": "Chicago",
                "zip_code": "60647",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "Chicago, IL 60647"
                ]
            },
            "phone": "+13127183295",
            "display_phone": "(312) 718-3295",
            "distance": 4088.3473278534216
        },
        {
            "id": "Ojj6ReF9Oc5UJ-W-mRI7WQ",
            "alias": "blind-robin-chicago",
            "name": "Blind Robin",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/NRqkI_XCs2f9t5vmOOfS_g/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/blind-robin-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 117,
            "categories": [
                {
                    "alias": "bars",
                    "title": "Bars"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 41.89731,
                "longitude": -87.68654
            },
            "transactions": [],
            "price": "$",
            "location": {
                "address1": "853 N Western Ave",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "853 N Western Ave",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17733953002",
            "display_phone": "(773) 395-3002",
            "distance": 1141.6784017207408
        },
        {
            "id": "NLrjoiMNPwpftp1TzdtW9g",
            "alias": "j-and-m-tap-chicago",
            "name": "J&M Tap",
            "image_url": "https://s3-media1.fl.yelpcdn.com/bphoto/AkiyEXukDlctOD4dznlOSA/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/j-and-m-tap-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 56,
            "categories": [
                {
                    "alias": "sportsbars",
                    "title": "Sports Bars"
                },
                {
                    "alias": "divebars",
                    "title": "Dive Bars"
                }
            ],
            "rating": 4,
            "coordinates": {
                "latitude": 41.8992344,
                "longitude": -87.6818573
            },
            "transactions": [],
            "price": "$",
            "location": {
                "address1": "957 N Leavitt",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "957 N Leavitt",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17732350499",
            "display_phone": "(773) 235-0499",
            "distance": 733.2682412525456
        },
        {
            "id": "NMfvUzRbNu5bUt4E_psKfg",
            "alias": "macs-american-pub-chicago",
            "name": "Mac's American Pub",
            "image_url": "https://s3-media4.fl.yelpcdn.com/bphoto/q_50nIFt__jZc9kcW8_Bkg/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/macs-american-pub-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 183,
            "categories": [
                {
                    "alias": "tradamerican",
                    "title": "American (Traditional)"
                },
                {
                    "alias": "pubs",
                    "title": "Pubs"
                }
            ],
            "rating": 3,
            "coordinates": {
                "latitude": 41.9031375,
                "longitude": -87.6725591
            },
            "transactions": [],
            "price": "$$",
            "location": {
                "address1": "1801 W Division St",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60622",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "1801 W Division St",
                    "Chicago, IL 60622"
                ]
            },
            "phone": "+17737824400",
            "display_phone": "(773) 782-4400",
            "distance": 485.94917433665086
        },
        {
            "id": "TyDF_Ae-b5jKukp8rpYDiQ",
            "alias": "whaddayaknow-free-team-trivia-chicago",
            "name": "Whaddayaknow Free Team Trivia",
            "image_url": "https://s3-media2.fl.yelpcdn.com/bphoto/giwZaiFn5wu4HW2SVOwIHw/o.jpg",
            "is_closed": false,
            "url": "https://www.yelp.com/biz/whaddayaknow-free-team-trivia-chicago?adjust_creative=UTB31bVlT1-JOtD4kS68ZA&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=UTB31bVlT1-JOtD4kS68ZA",
            "review_count": 34,
            "categories": [
                {
                    "alias": "triviahosts",
                    "title": "Trivia Hosts"
                }
            ],
            "rating": 5,
            "coordinates": {
                "latitude": 41.9489531,
                "longitude": -87.6442228
            },
            "transactions": [],
            "location": {
                "address1": "",
                "address2": "",
                "address3": "",
                "city": "Chicago",
                "zip_code": "60613",
                "country": "US",
                "state": "IL",
                "display_address": [
                    "Chicago, IL 60613"
                ]
            },
            "phone": "+16178690487",
            "display_phone": "(617) 869-0487",
            "distance": 5604.771778831169
        }
    ],
    "total": 58,
    "region": {
        "center": {
            "longitude": -87.67776489257812,
            "latitude": 41.90515925618311
        }
    }
}